# Generated by Django 4.1 on 2022-08-18 16:04

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('tg', '0005_alter_users_birthday'),
    ]

    operations = [
        migrations.AddField(
            model_name='log',
            name='place',
            field=models.CharField(blank=True, default='start', max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='users',
            name='update_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
