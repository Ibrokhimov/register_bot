# Generated by Django 4.1 on 2022-08-18 16:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tg', '0006_log_place_users_update_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='is_admin',
            field=models.BooleanField(default=False),
        ),
    ]
