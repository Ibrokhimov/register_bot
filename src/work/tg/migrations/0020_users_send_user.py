# Generated by Django 4.1 on 2022-11-14 14:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tg', '0019_alter_users_admin_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='send_user',
            field=models.BooleanField(default=False, help_text='Bu belgilansa admin text dagi xabar bilan lokatsiya userga boradi'),
        ),
    ]
