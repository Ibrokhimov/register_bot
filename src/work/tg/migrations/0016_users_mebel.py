# Generated by Django 4.1 on 2022-11-04 12:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tg', '0015_specialty_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='mebel',
            field=models.CharField(blank=True, default='HA', max_length=100, null=True),
        ),
    ]
