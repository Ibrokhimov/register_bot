from django.core.management.base import BaseCommand, CommandError
import json
from work.tg.models import Region, District, Specialty


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('action', type=str, help='Indicates action')

    def handle(self, *args, **options):
        action = options.get("action")
        if action == 'region':
            with open('region.json', 'r', encoding='utf-8') as f:
                data = json.loads(f.read())
                for i, x in enumerate(data, start=1):
                    region = Region.objects.create(name=x.get('name_uz', ''), sort=i)
                    with open('district.json', 'r', encoding='utf-8') as f2:
                        districts = json.loads(f2.read())
                        for j, district in enumerate(districts, start=1):
                            if district.get('region_id') == x.get('id'):
                                District.objects.create(name=district.get('name_uz', '--'), sort=j, region=region)

            print("---------------- Region District Tugadi ------------")
