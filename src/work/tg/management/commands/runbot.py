from django.core.management.base import BaseCommand
from telegram.ext import (messagequeue as mq, Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler)
from telegram.utils.request import Request
from django.conf import settings
from ...mqbot import MQBot

from ...views import start, message_handler, contact_handler, all_handler, callback_handler, photo_handler, \
    video_handler, document_handler, location_handlar


class Command(BaseCommand):
    help = 'Mebel sexining ishga olish boti'

    def handle(self, *args, **options):
        q = mq.MessageQueue(all_burst_limit=3, all_time_limit_ms=3000)
        request = Request(con_pool_size=36)

        bot = MQBot(settings.TOKEN_KEY, request=request, mqueue=q)
        updater = Updater(bot=bot, use_context=True, workers=32)

        dispatcher = updater.dispatcher

        dispatcher.add_handler(CommandHandler("start", start))
        dispatcher.add_handler(MessageHandler(Filters.text, message_handler))
        dispatcher.add_handler(MessageHandler(Filters.contact, contact_handler))
        dispatcher.add_handler(MessageHandler(Filters.photo, photo_handler))
        dispatcher.add_handler(MessageHandler(Filters.video, video_handler))
        dispatcher.add_handler(MessageHandler(Filters.document, document_handler))

        dispatcher.add_handler(CallbackQueryHandler(callback_handler))
        dispatcher.add_handler(MessageHandler(Filters.location, location_handlar))
        dispatcher.add_handler(MessageHandler(Filters.all, all_handler))

        updater.start_polling()
        updater.idle()
