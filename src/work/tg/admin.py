from django.contrib import admin
from .models import Log, Specialty, Region, District, Users


class RegionAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'is_active']


class UserAdmin(admin.ModelAdmin):
    exclude = ("file_id", 'latitude', 'longitude')
    list_display = ['first_name', 'last_name', 'phone_number', 'birthday', 'mebel', 'specialty', 'is_active',
                    'check_admin',
                    'file', 'created_at']
    search_fields = ["created_at__date", "first_name", "last_name", "phone_number", "specialty__name"]


class LogAdmin(admin.ModelAdmin):
    list_display = ['pk', 'tg_id', 'place', 'created_at']
    search_fields = ['place', 'tg_id']


class SpecialtyAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'message']


admin.site.register(Log, LogAdmin)
admin.site.register(Specialty, SpecialtyAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(District)
admin.site.register(Users, UserAdmin)
