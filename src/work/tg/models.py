from django.db import models

from telegram import bot
from django.conf import settings

BOT = bot.Bot(settings.TOKEN_KEY)


def default_json():
    return {}


class Region(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    sort = models.IntegerField(default=1)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.name or '+++++'

    class Meta:
        verbose_name_plural = "Viloyatlar"


class District(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE, related_name="region_district")
    name = models.CharField(max_length=100, null=False, blank=False)
    sort = models.IntegerField(default=1)

    def __str__(self):
        return self.name or "------"

    class Meta:
        verbose_name_plural = "Tumanlar"


class Specialty(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    message = models.CharField(max_length=200, null=False, blank=False)
    sort = models.IntegerField(default=1)

    def __str__(self):
        return self.name or self.message

    class Meta:
        verbose_name_plural = "Mutaxassisliklar"


class Users(models.Model):
    tg_id = models.BigIntegerField(null=True, blank=True)
    first_name = models.CharField(max_length=100, null=False, blank=False)
    last_name = models.CharField(max_length=100, null=False, blank=False)
    birthday = models.DateField(null=False, blank=False)
    mebel = models.CharField(max_length=100, null=True, blank=True, default="HA")
    specialty = models.ForeignKey(Specialty, related_name="users_specialty", null=True, blank=True,
                                  on_delete=models.SET_NULL)
    region = models.ForeignKey(Region, related_name="users_region", null=True, blank=True, on_delete=models.SET_NULL)
    district = models.ForeignKey(District, related_name="users_district", null=True, blank=True,
                                 on_delete=models.SET_NULL)
    phone_number = models.CharField(max_length=100, null=False, blank=False)

    file = models.FileField(upload_to="files", null=True, blank=True)
    file_id = models.CharField(max_length=300, null=True, blank=True)

    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)
    is_active = models.BooleanField(default=False)
    check_admin = models.BooleanField(default=False)
    admin_text = models.TextField(null=True, blank=True, default="Uzur sizning nomzodingiz bizga mos kelmadi.",
                                  help_text="Foydalanuvchiga boradigan xabar")
    send_text = models.BooleanField(default=False,
                                    help_text="Bu belgilansa admin text dagi xabar foydalanuvchiga boradi")
    send_location = models.BooleanField(default=False,
                                        help_text="Bu suhbatga chaqirilmoqchi bo'lgan Foydalanuvchiga lokatsiya yuborish uchun belgilanadi(Lokatsiya yuborilmoqchi bo'lsa shu belgilanadi)")

    latitude = models.FloatField(null=True, blank=True, default=41.353824)
    longitude = models.FloatField(null=True, blank=True, default=69.146884)

    def __str__(self):
        return self.first_name or self.last_name

    def save(self, *args, **kwargs):
        if self.admin_text and self.send_text:
            message = f"<b>{self.admin_text}</b>"
            try:
                BOT.send_message(f'{self.tg_id}', message, parse_mode='HTML')
            except Exception as e:
                print("error send text e=", e)
        if self.send_location and self.latitude and self.longitude:
            try:
                BOT.send_location(chat_id=f'{self.tg_id}', latitude=self.latitude, longitude=self.longitude)
            except Exception as e:
                print("error location e=", e)
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Foydalanuvchilar"


class Log(models.Model):
    tg_id = models.BigIntegerField(primary_key=True, null=False, )
    messages = models.JSONField(blank=True, null=True, default=default_json, )
    place = models.CharField(max_length=100, null=True, blank=True, default='start')

    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return "#%s" % self.tg_id
