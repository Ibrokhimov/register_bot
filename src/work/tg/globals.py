USER_TEXTS = {
    'first_name': {
        'title': '<b>👤 Ism: </b>',
        'text': "Ismingizni kiriting 👇",
        'example': "<b>Misol:</b> Bobur",
    },
    'last_name': {
        'title': '<b>👤 Familiya: </b>',
        'text': 'Familiyangizni kiriting 👇',
        'example': '<b>Misol:</b> Suyunov'
    },
    'age': {
        'title': "<b>🕰 Tug'ilgan kun: </b>",
        'text': "Tug'ilgan kuningizni kiriting 👇",
        'example': '<b>Misol:</b> 21.05.2001'
    },
    'region': {
        'title': '<b>🌐 Viloyat: </b>',
        'text': 'Viloyatingizni tanlang 👇',
        'example': ''
    },
    'district': {
        'title': '<b>🌐 Tuman: </b>',
        'text': 'Tumanni tanlang 👇',
        'example': ''
    },
    'phone_number': {
        'title': '<b>📞 Aloqa: </b>',
        'text': 'Telefon nomeringizni kiriting 👇',
        'example': '<b>Misol:</b> +998991234567'
    },
    'level_education': {
        'title': "<b>📰 Mebel: </b>",
        'text': "Mebel sohasida ishlaganmisiz tanlang 👇",
        'example': ''
    },
    'specialty': {
        'title': '<b>📑 Mutaxassisligi: </b>',
        'text': 'Mutaxassisligingizni tanlang 👇',
        'example': ''
    },
    'file_id': {
        'title': '<b>🎬 File: </b>',
        'text': "Biz sizni qobiliyatlaringizni ko'rmoqchimiz 👇\n",
        'example': ""
    }
}

AD = {
    'file_id': 'AgACAgIAAxkBAAIbYWL6ULFlOmA-AgevY7wHvUdvblNcAAJ5tDEbinJhSR-fb-SFF-58AQADAgADeQADKQQ',
    'text': f"""
        <b>Har bir inson o’ziga yoqqan ish topishni orzu qiladi.</b>

Ish jarayonida uzluksiz rivojlanishda esa unga ishda doimo yangi qirralarni kashf etish va ularni zabt etish yordam beradi.
Eng asosiysi esa u oddiygina hodim emas, balki <b>ELCHI bo’lishidadir.</b>

<b>✅ Elchi</b>
Bozorlar, Do’konlar va Aptekalarda Mijozlarimiz bilan ishlaydi, mahsulotlarni tushuntiradi va kassada ham ishlashi mumkin.

<b>✅Supervayzer</b>
Ishni tashkillashtiradi, mijozlar sifatli maslahat olishini va elchilarni rivojlantirishni ta’minlaydi.

<b>*Sinov Muddati(1-2oy) paytida 1.2mln stipendiya beriladi.
*Oyliklar 2mlndan boshlanadi</b>

<b>Ishga qabul qilish boti orqali anketa to’ldirishingiz mumkin:</b>
https://t.me/novatioish_bot
    """
}
