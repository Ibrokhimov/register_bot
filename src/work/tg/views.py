import json
from django.conf import settings
import datetime
from telegram import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton, BotCommand, KeyboardButton, \
    ReplyKeyboardRemove

from .models import Users, Log, Region, District, Specialty
from .globals import USER_TEXTS


# --------------- sends function ------------------------------------- #

def go_message(context, user_id, message, reply_markup=None):
    context.bot.send_message(chat_id=user_id, text=message, reply_markup=reply_markup, parse_mode='HTML',
                             disable_web_page_preview=True)


def send_photo(context, user_id, photo, caption=None, reply_mukup=None):
    try:
        context.bot.send_photo(chat_id=user_id, photo=photo, caption=caption, reply_markup=reply_mukup,
                               parse_mode='HTML')
    except Exception as e:
        print("error send_photo", e)


def send_video(context, user_id, message, video, buttons=None):
    data = context.bot.send_video(chat_id=user_id, video=video, caption=message, reply_markup=buttons,
                                  parse_mode='HTML', )
    return data


def send_document(context, user_id, message, document, buttons=None):
    context.bot.send_document(chat_id=user_id, caption=message, document=document, reply_markup=buttons,
                              parse_mode="HTML")


def delete_message_user(context, user_id, message_id):
    context.bot.delete_message(chat_id=user_id, message_id=message_id)


def send_audio(context, user_id, message, audio, buttons):
    try:
        context.bot.send_audio(chat_id=user_id, audio=audio, caption=message, reply_markup=buttons,
                               parse_mode='HTML')
    except Exception as e:
        print("error", e)


def edit_message(context, chat_id, message_id, message, reply_markup):
    try:
        context.bot.edit_message_text(
            chat_id=chat_id, message_id=message_id, text=message,
            reply_markup=reply_markup, parse_mode='HTML'
        )
    except Exception as e:
        print('ERROR edit message: ', str(e))


class UserData:

    def __init__(self, tg_id):
        self.tg_id = tg_id
        try:
            self.log = Log.objects.get(tg_id=tg_id)
        except Exception as e:
            self.log = Log.objects.create(tg_id=tg_id, messages={})

    def get_state(self):
        message = self.log.messages
        return message

    def change_state(self, messages):
        self.log.messages = messages
        self.log.save()

    def change_place_user(self, place='start'):
        self.log.place = place
        self.log.save()

    def clear_state(self, state=0):
        self.log.messages = {'state': state, 'menu_state': 1}
        self.log.save()


# --------------------------  end sends function ----------------------------------------
BUTTON = ["✍️Ro'yxatdan o'tish", '⬅️ Ortga']
KEYS = [
    'first_name',
    'last_name',
    'age',
    'region',
    'district',
    'phone_number',
    'level_education',
    'specialty',
    'file_id'
]

MEDIA_ROOT = settings.MEDIA_ROOT


def start(update, context):
    user_data = update.message.from_user

    message = f"<b>Assalomu aleykum </b><a href='tg://user?id={user_data.id}'>{user_data.first_name or '----'}</a>"
    go_message(context, user_data.id, message,
               reply_markup=ReplyKeyboardMarkup([[BUTTON[0]]], one_time_keyboard=True, resize_keyboard=True))

    return 1


def date_check(date):
    date_format = '%d.%m.%Y'
    try:
        date_obj = datetime.datetime.strptime(date, date_format)
        return date_obj
    except ValueError:
        return False


def message_handler(update, context):
    command = [BotCommand("start", "Boshlash")]
    context.bot.set_my_commands(command)

    user_data = update.message.from_user

    user_log = UserData(user_data.id)
    user_state = user_log.get_state()
    text = update.message.text
    message_id = update.message.message_id

    if text == BUTTON[0]:
        user_state = {'menu_state': 1, 'state': 0, 'data': {}}

    menu_state = user_state.get('menu_state', 1)
    state = user_state.get('state', 0)

    if menu_state == 1:
        if state == 0:
            user_state['state'] = 100
            user_log.change_state(user_state)

            question = USER_TEXTS['first_name']
            message = f"{question['title']}\n\n{question['text']}\n{question['example']}"
            go_message(context, user_id=user_data.id, message=message)
        else:
            if text == BUTTON[1]:
                index = user_state.get('index', 0)
                if KEYS[index] == 'phone_number':
                    user_state.update({'index': int(index) - 2})
                elif index != 0:
                    user_state.update({'index': int(index) - 2})
                elif index == 0 or index == -1:
                    user_state['index'] = 0
                    user_state['data'] = {}
                    user_log.change_state(user_state)
                    question = USER_TEXTS['first_name']
                    message = f"{question['title']}\n\n{question['text']}\n{question['example']}"
                    go_message(context, user_id=user_data.id, message=message)
                    return 1

            registratsion_function(context=context, user_data=user_data, user_log=user_log, text=text,
                                   user_state=user_state, message_id=message_id)


def registratsion_function(context, user_data, user_log, text, user_state, message_id):
    x = False
    specialty = None

    state = user_state.get('state', 0)
    index = user_state.get('index', 0)
    data = user_state.get('data', {})

    if state == 200:
        if text == '✅ HA':
            user_log.change_place_user('admin')
            go_message(context, user_id=user_data.id,
                       message="<b>✅ Ma'lumotlaringiz operatorlarimizga yuborildi.\n\nSizning ma'lumotlaringiz adminlar tomonidan ko'rib chiqiladi!</b>",
                       reply_markup=ReplyKeyboardRemove())
            user_create(tg_id=user_data.id, data=data, context=context)

        elif text == '❌ YOQ':
            user_state['state'] = 300
            user_log.change_state(user_state)
            button = []
            message = "<b>Qaysi malumotingizni o'zgartirmoqchisiz?</b>"
            for key, value in data.items():
                if key not in ['file_id', 'file_type', 'format']:
                    title = USER_TEXTS[key]['title']
                    button.append([InlineKeyboardButton(text=f"{title.split()[1]} {value}\n",
                                                        callback_data=f"edit#{key}")])

            button.append([InlineKeyboardButton(text="🎬 File: ", callback_data="edit#file_id")])
            go_message(context, user_id=user_data.id, message=message, reply_markup=InlineKeyboardMarkup(button))

        else:
            delete_message_user(context=context, user_id=user_data.id, message_id=message_id)
            go_message(context=context, user_id=user_data.id, message=f"<b>Buttonlardan birini tanlang 👇</b>")

        return 1

    if KEYS[index] == 'age':
        date_string = text
        date_format = '%d.%m.%Y'
        try:
            if text != BUTTON[1]:
                datetime.datetime.strptime(date_string, date_format)
                data[KEYS[index]] = text
            x = True
        except ValueError:
            message = f"<b>❌ Xato</b>\n\nMalumotni to'g'ri kiriting!\n<b>Misol:</> 21.05.2001"
            go_message(context=context, user_id=user_data.id, message=message)
            return 1

    elif KEYS[index] == 'level_education' and text != BUTTON[1]:
        levels = ['✅ HA', '❌ YOQ']
        if text in levels:
            x = True
            data[KEYS[index]] = text
        else:
            message = f"<b>❌ Xato</b>\n\n Pastdagi buttonlardan foydalaning!!"
            go_message(context=context, user_id=user_data.id, message=message)
            return 1

    elif KEYS[index] == 'specialty' and text != BUTTON[1]:
        specialty = Specialty.objects.filter(name=text).first()
        if specialty:
            x = True
            data[KEYS[index]] = text
        elif text != BUTTON[1]:
            message = f"<b>❌ Xato</b>\n\n Pastdagi buttonlardan foydalaning!!"
            go_message(context=context, user_id=user_data.id, message=message)
            return 1

    elif KEYS[index] == 'region' and text != BUTTON[1]:
        region = Region.objects.filter(name=text, is_active=True).first()
        if region:
            x = True
            data[KEYS[index]] = text
        elif text != BUTTON[1]:
            message = f"<b>❌ Xato</b>\n\n Pastdagi buttonlardan foydalaning!!"
            go_message(context=context, user_id=user_data.id, message=message)
            return 1

    elif KEYS[index] == 'district' and text != BUTTON[1]:
        district = District.objects.filter(region__name=data.get('region', '')).filter(name=text)
        if district:
            x = True
            data[KEYS[index]] = text
        elif text != BUTTON[1]:
            message = f"<b>❌ Xato</b>\n\n Pastdagi buttonlardan foydalaning!!"
            go_message(context=context, user_id=user_data.id, message=message)
            return 1

    elif KEYS[index] == 'phone_number' and text != BUTTON[1]:
        phone_number = check_phone_number(text)
        if phone_number and phone_number[1]:
            x = True
            data[KEYS[index]] = phone_number[0]
        else:
            message = f"<b>❌ Xato</b>\n\n Telefon raqamingizni to'g'ri kiriting!"
            go_message(context, user_id=user_data.id, message=message)

    elif KEYS[index] == 'file_id' and index != -1 and text != BUTTON[1]:
        message = f"<b>❌ Xato</b>\n\n Siz File yuklashingiz kerak"
        go_message(context=context, user_id=user_data.id, message=message)
        return 1

    if not x and text != BUTTON[1]:
        data[KEYS[index]] = text

    if state != 300 or (state == 300 and KEYS[index] == 'region'):
        user_state['data'] = data
        user_state['index'] = index + 1
        user_log.change_state(user_state)

        buttons = render_button(register_data=data, key=KEYS[index + 1])

        question = USER_TEXTS[KEYS[index + 1]]
        user_log.change_place_user(KEYS[index + 1])

        question_text = specialty.message if specialty else question['text']

        message = f"{question['title']}\n\n{question_text}\n{question['example']}"
        go_message(context=context, user_id=user_data.id, message=message, reply_markup=buttons)

    elif state == 300:
        file_type = data.get('file_type')
        user_state['state'] = 200
        user_state['data'] = data
        user_log.change_state(user_state)
        to_ask_user(user_state=user_state, context=context, user_data=user_data, file_type=file_type)


def user_create(tg_id, data, context):
    date_format = '%d.%m.%Y'
    try:
        date = datetime.datetime.strptime(data.get('age', '----'), date_format)
    except:
        date = datetime.datetime.today()

    user = Users.objects.create(
        tg_id=int(tg_id),
        first_name=data.get('first_name', '--'),
        last_name=data.get('last_name', '----'),
        birthday=date,
        mebel=data.get('level_education', 'Yoq'),
        specialty=Specialty.objects.filter(name=data.get('specialty', '----')).first(),
        region=Region.objects.filter(name=data.get('region', '----')).first(),
        district=District.objects.filter(name=data.get('district', '----')).first(),
        phone_number=data.get('phone_number', '--'),
        is_active=False,
        check_admin=False
    )
    file_id = data.get('file_id')
    file_type = data.get('file_type', 'photo')
    if file_type and file_id:
        if file_type == 'photo':
            file = context.bot.getFile(file_id=file_id)
            file.download(f'{MEDIA_ROOT}/files/user-{tg_id}.jpg')
            file = f"files/user-{tg_id}.jpg"
            user.file = file

        elif file_type == 'video':
            file = context.bot.getFile(file_id=file_id)
            file.download(f'{MEDIA_ROOT}/files/user-{tg_id}.mp4')
            file = f"files/user-{tg_id}.mp4"
            user.file = file

        elif file_type == "document":
            form = data.get('format', '.pdf')
            file = context.bot.getFile(file_id=file_id)
            file.download(f'{MEDIA_ROOT}/files/user-{tg_id}{form}')
            file = f"files/user-{tg_id}{form}"
            user.file = file

        user.file_id = file_id
        user.save()


def contact_handler(update, context):
    user_data = update.message.from_user

    user_log = UserData(user_data.id)
    user_state = user_log.get_state()

    state = user_state.get('state', 0)
    index = user_state.get('index', 0)
    data = user_state.get('data', {})

    phone_number = update.message.contact.phone_number
    phone_number = check_phone_number(phone_number)

    if KEYS[index] == 'phone_number':
        if phone_number and phone_number[1]:
            data[KEYS[index]] = phone_number[0]

            user_state['data'] = data
            user_state['index'] = index + 1
            user_log.change_state(user_state)

            buttons = render_button(register_data=data, key=KEYS[index + 1])

            question = USER_TEXTS[KEYS[index + 1]]
            user_log.change_place_user(KEYS[index + 1])

            message = f"{question['title']}\n\n{question['text']}\n{question['example']}"
            go_message(context=context, user_id=user_data.id, message=message, reply_markup=buttons)

        else:
            message = f"<b>❌ Xato</b>\n\n Telefon raqamingizni to'g'ri kiriting!"
            go_message(context, user_id=user_data.id, message=message)


def to_ask_user(user_state, context, user_data, file_type):
    data = user_state.get('data', {})

    message = ""
    for key in KEYS:
        if key not in ['file_id', "file_type"]:
            value = data.get(key) or '----'
            message += f"{USER_TEXTS[key]['title']} {value}\n"

    if file_type == "photo":
        send_photo(context=context, user_id=user_data.id, photo=data.get('file_id'), caption=message)
    elif file_type == "video":
        send_video(context=context, user_id=user_data.id, video=data.get('file_id'), message=message)
    elif file_type == "document":
        send_document(context=context, user_id=user_data.id, document=data.get('file_id'), message=message)

    message = f"""<b>Barcha malumotlaringiz to'g'rimi?</b>\n\n"""
    button = ReplyKeyboardMarkup([
        ['✅ HA'],
        ['❌ YOQ']
    ], one_time_keyboard=True, resize_keyboard=True)
    go_message(context, user_id=user_data.id, message=message, reply_markup=button)


def render_button(register_data, key):
    but = []
    button = []

    data = []
    if key == 'specialty':
        data = Specialty.objects.all().order_by('sort')
    elif key == 'region':
        data = Region.objects.filter(is_active=True).order_by('sort')
    elif key == 'district':
        data = District.objects.filter(region__name=register_data['region'])
    elif key == 'level_education':
        button = [['✅ HA', '❌ YOQ']]
    if data:
        for i in data:
            but.append(i.name)
            if len(but) == 2:
                button.append(but)
                but = []
        if len(but) > 0:
            button.append(but)

    elif key == 'phone_number':
        button = [[KeyboardButton(text="📞 Raqamni yuborish", request_contact=True)]]

    button.append([BUTTON[1]])
    button = ReplyKeyboardMarkup(button, one_time_keyboard=True, resize_keyboard=True) if button else None
    return button


def check_phone_number(phone_number):
    phone_number = phone_number.replace(" ", "")
    if 0 < len(phone_number) < 14:
        if phone_number[0] == '+' and len(phone_number) == 13:
            if phone_number[1:-1].isdigit():
                return [phone_number, True]
        elif len(phone_number) == 9:
            if phone_number[:-1].isdigit():
                return [f"+998{phone_number}", True]

        elif len(phone_number) == 12:
            if phone_number[:-1].isdigit():
                return [f"+{phone_number}", True]
    return False


def photo_handler(update, context):
    user_data = update.message.from_user
    file_id = update.message.photo[-1].file_id

    user_log = UserData(user_data.id)
    user_state = user_log.get_state()

    index = int(user_state.get('index', 0))

    if KEYS[index] == 'file_id' and index != -1:
        user_state['data'].update({'file_id': file_id, 'file_type': "photo"})
        user_state['state'] = 200
        user_log.change_state(user_state)

        to_ask_user(user_state=user_state, context=context, user_data=user_data, file_type="photo")


def video_handler(update, context):
    user_data = update.message.from_user
    file_id = update.message.video.file_id

    user_log = UserData(user_data.id)
    user_state = user_log.get_state()

    index = int(user_state.get('index', 0))

    if KEYS[index] == 'file_id' and index != -1:
        user_state['data'].update({'file_id': file_id, 'file_type': "video"})
        user_state['state'] = 200
        user_log.change_state(user_state)

        to_ask_user(user_state=user_state, context=context, user_data=user_data, file_type="video")


def document_handler(update, context):
    file_name = update.message.document.file_name
    form = '.docx' if '.docx' in file_name else '.pdf'
    user_data = update.message.from_user
    file_id = update.message.document.file_id

    user_log = UserData(user_data.id)
    user_state = user_log.get_state()

    index = int(user_state.get('index', 0))

    if KEYS[index] == 'file_id' and index != -1:
        user_state['data'].update({'file_id': file_id, 'file_type': "document", "format": form})
        user_state['state'] = 200
        user_log.change_state(user_state)

        to_ask_user(user_state=user_state, context=context, user_data=user_data, file_type="document")


def all_handler(update, context):
    user_data = update.message.from_user
    user_log = UserData(user_data.id)
    user_state = user_log.get_state()

    index = int(user_state.get('index', 0))

    if KEYS[index] == 'file_id' and index != -1:
        message = f"<b>❌ Xato</b>\n\n Siz So'ralgan File ni yuklashingiz kerak"
        go_message(context=context, user_id=user_data.id, message=message)
        return 1


def callback_handler(update, context):
    query = update.callback_query
    user_data = query.from_user
    message_id = query.message.message_id
    data_sp = query.data.split('#')

    user_log = UserData(user_data.id)
    user_state = user_log.get_state()
    data = user_state.get('data', {})

    if data_sp[0] == 'edit':
        index = KEYS.index(data_sp[1])
        user_state.update({'index': index})
        user_log.change_state(user_state)

        question = USER_TEXTS[data_sp[1]]
        buttons = render_button(register_data=data, key=data_sp[1])
        message = f"{question['title']}\n\n{question['text']}\n{question['example']}"
        delete_message_user(context, user_id=user_data.id, message_id=message_id)
        go_message(context=context, user_id=user_data.id, message=message, reply_markup=buttons)


def location_handlar(update, context):
    pass
    # user_data = update.message.from_user
    # location = update.message.location
    #
    # message = f"<b>📍 Sizning locatsiya malumotlaringiz</b>\n\nLongitude: {location.longitude}\nLatitude: {location.latitude}"
    # go_message(context=context, user_id=user_data.id, message=message)
